window.seqUrl = "http://localhost:5341/api/events/raw";
window.apiKey = "C2dpgxIEF1ZPCXS7pFs";

window.SDS_IP_ADDRESSES = [
    "sds-dev-cloud.azurewebsites.net"
    // "192.168.0.220/SynectDataServer"
];
window.SDS_API_KEY = ""; // Not currently used
window.SDS_TIMEOUT = 3000; // If the SDS doesn't respond within this amount of time, consider it a failed reqeust
window.SDS_INTERVAL_TIME = 100; // Amount of time to wait before trying the next SDS if the pervious one failed
window.CHECKIN_STATUS_STYLE = "text"; // Options are "circle" and "text" - this is only for NFRA checkin video wall
window.WAITING_AREA_STATUS_STYLE = "circle"; // Options are "circle" and "text" - this is only for NFRA waiting area screen

window.IMAGE_SERVER_URL = "http://localhost:53713";
window.IMAGE_SERVER_UPLOAD_PATH = "/ImageBank/UploadBinaryImage";
window.IMAGE_SERVER_TRANSACTION_START_PATH = "/ImageBank/TransactionStart";
window.IMAGE_SERVER_TRANSACTION_END_PATH = "/ImageBank/TransactionEnd";

window.IMAGE_HEARTBEAT_SERVER_URL = "http://localhost:9000";
window.IMAGE_SERVER_HEARTBEAT_PATH = "/api/values/signal";

window.htmlItems = [
    {
        "Airline": "SunCountry",
        "Comp": "Checkin",
        "Units": [2,3]
    },

    {
        "Airline": "Bahamas",
        "Comp": "Checkin",
        "Units": [2,3]
    }
];

window.HTML_RESTART_DELAY = 30000; // milliseconds to wait after an image is uploaded before loading next HTMl item
