var app = require('electron').remote;
var nodeConsole = require('console');
var myConsole = new nodeConsole.Console(process.stdout, process.stderr);
var dialog = app.dialog;
var fs = require('fs');
var filePath;
var mainHeaders = 0;
var parsedJson;
var airline;
var headerContentX;
var headerContentY;
var contents;

document.getElementById('makeChanges').disabled = true;

document.getElementById('file').addEventListener('change', readSingleFile, false);





var map = [];
map["SCX"] = "SunCountry";
map["AMX"] = "AeroMexico";
map["BER"] = "AirBerlin";
map["BHS"] = "Bahamas";
map["BWA"] = "Caribbean";
map["BTA"] = "EuroWings";
map["FI"] = "IcelandAir";
map["JBU"] = "JetBlue";
map["DLH"] = "Lufthansa";
map["SIL"] = "Silver";
map["NK"] = "Spirit";
map["UA"] = "UnitedAirlines";

function retrieveFileName(path){
	while(path.includes("\\")){
	path = path.substring(path.indexOf("\\")+1);
	}
	return path;
}

// function toggleDisplay(){
// 	var head = document.getElementById('header1');
//     if (head.style.display === 'none') {
//         head.style.display = 'block';
//     } else {
//         head.style.display = 'none';
//     }
// }


// document.getElementById('createButton').onclick = () => {
// 	dialog.showSaveDialog((nu)  => {
// 		if(nu === undefined){
// 			alert("You didn't save file");
// 			return;
// 		}
// 		var content = "hello there";
// 		fs.writeFile(nu, content, (err) => {
// 			if(err) console.log(err);
// 			alert("JSON File was saved");
//
// 		})
// 	});
// };

document.addEventListener("keydown", function (e) {
		if (e.which === 123) {
			require('remote').getCurrentWindow().toggleDevTools();
		} else if (e.which === 116) {
			location.reload();
		}
	});

function readSingleFile(e) {
	document.getElementById('makeChanges').style.cursor = "pointer";
	document.getElementById('makeChanges').disabled = false;
  filePath = e.target.files[0].path;
	var jsonFile = fs.readFileSync(filePath,"utf8");

	//gets and parses the json file
	parsedJson = JSON.parse(jsonFile);
	//splits url in json to find abbreviation of flight
	var flightAbbr = parsedJson.Sources[0].url.split("/")[4];
	var airline = map[flightAbbr];
	document.getElementById('notice').innerHTML = "You are editing the JSON file for " + airline ;
	var indArray = [];
	var headerNames = [];
	//gets all elements into one array
	for(var i=0; i < parsedJson.Elements.length ; i++) {
	indArray.push(parsedJson.Elements[i]);
	if(indArray[i].textcontent.text != ""){
		//changes header names in html
		document.getElementById("header"+(i+1)).innerHTML = indArray[i].textcontent.text.toUpperCase();
		document.getElementById("header"+(i+1)).style.display="block";
		document.getElementById("form"+(i+1)).style.display = "block";
		headerNames.push(indArray[i].textcontent.text);
		//increase amount of headers
		mainHeaders++;
	}
	}
		for(var c = 0; c<mainHeaders; c++){
			headerContentX = parsedJson.Elements[c].x;
			headerContentY = parsedJson.Elements[c].y;
			document.getElementById("header"+(c+1)+"X").value = headerContentX;
			document.getElementById("header"+(c+1)+"Y").value = headerContentY;
		}
  }


document.getElementById('makeChanges').onclick = () => {
	document.getElementById('makeChanges').value = "JSON FILE UPDATED";
	//will add header for multiplication purposes
		for(var c = 0; c<mainHeaders; c++){
			//5 because we only need to change it 5 times in the json. once for header and other times for flight
			for(var d = 0; d<5; d++){
				headerContentX = document.getElementById("header"+(c+1)+"X").value;
				headerContentY = document.getElementById("header"+(c+1)+"Y").value;

				if(headerContentX){
					parsedJson.Elements[c+(d*mainHeaders)].x = parseInt(headerContentX);
				}
				if(headerContentY){
					parsedJson.Elements[c+(d*mainHeaders)].y = parseInt(headerContentY);
				}
			}
			counter=0;
		}

		var data = JSON.stringify(parsedJson, undefined, 2);
		fs.writeFileSync(filePath, data);
		return true;

};
