	var app = require('electron').remote;
	var nodeConsole = require('console');
	var myConsole = new nodeConsole.Console(process.stdout, process.stderr);
	var dialog = app.dialog;
	var fs = require('fs');
	var filePath;
	var mainHeaders = 0;
	var parsedJson;
	var airline;
	var headerContentX;
	var headerContentY;
	var flightsContentX;
	var flightsContentY;
	var contents;
  var threeJsonBool = false;
	var onOrOff = false;
	var xArr = [];
	var yArr = [];
	var deltaY;


	// const {webFrame} = require('electron');
	// webFrame.setZoomFactor(1.5);
	const webview = document.querySelector('webview');

	const BrowserWindow = app.BrowserWindow;
	var sideWindow;



	document.getElementById('makeChanges').disabled = true;

	document.getElementById('file').addEventListener('change', readSingleFile, false);





	var map = [];
	map["SCX"] = "SunCountry";
	map["AMX"] = "AeroMexico";
	map["BER"] = "AirBerlin";
	map["BHS"] = "Bahamas";
	map["BWA"] = "Caribbean";
	map["BTA"] = "EuroWings";
	map["FI"] = "IcelandAir";
	map["JBU"] = "JetBlue";
	map["DLH"] = "Lufthansa";
	map["SIL"] = "Silver";
	map["NK"] = "Spirit";
	map["UA"] = "UnitedAirlines";

	// function toggleDisplay(){
	// 	var head = document.getElementById('header1');
	//     if (head.style.display === 'none') {
	//         head.style.display = 'block';
	//     } else {
	//         head.style.display = 'none';
	//     }
	// }


	// document.getElementById('createButton').onclick = () => {
	// 	dialog.showSaveDialog((nu)  => {
	// 		if(nu === undefined){
	// 			alert("You didn't save file");
	// 			return;
	// 		}
	// 		var content = "hello there";
	// 		fs.writeFile(nu, content, (err) => {
	// 			if(err) console.log(err);
	// 			alert("JSON File was saved");
	//
	// 		})
	// 	});
	// };

	document.addEventListener("keydown", function (e) {
			if (e.which === 123) {
				require('remote').getCurrentWindow().toggleDevTools();
			} else if (e.which === 116) {
				location.reload();
			}
		});


	function readSingleFile(e) {
		//if choose file is chosen again before submitting
		mainHeaders=0;

		//end pre conditions
		document.getElementById('makeChanges').style.cursor = "pointer";
		document.getElementById('makeChanges').disabled = false;
	  filePath = e.target.files[0].path;
		var jsonFile = fs.readFileSync(filePath,"utf8");

		//gets and parses the json file
		parsedJson = JSON.parse(jsonFile);

		//is 3.json selected
		//splits url in json to find abbreviation of flight
		var flightAbbr = parsedJson.Sources[0].url.split("/")[4];
		var airline = map[flightAbbr];
		// sideWindow = new BrowserWindow({width:600, height:600, frame: false, x:0, y:0, focusable: false});
		document.getElementById('wig1').src = "http://192.168.0.20:8080" + "?airline=" + airline + "&comp=Checkin&s=2";
	  document.getElementById('wig2').src = "http://192.168.0.20:8080" + "?airline=" + airline + "&comp=Checkin&s=3";

		document.getElementById('notice').innerHTML = "You are editing the JSON file for " + airline ;

		var indArray = [];
		var headerNames = [];


		//gets all elements into one array
		for(var i=0; i < parsedJson.Elements.length ; i++) {
		indArray.push(parsedJson.Elements[i]);
		//hides all the header and forms if newfile selected
		//in case user selects a file with 3 headers and then a files only 2 headers right after.
		if(document.getElementById("header"+(i+1)) && document.getElementById("form"+(i+1))){
		document.getElementById("header"+(i+1)).style.display="none";
		document.getElementById("form"+(i+1)).style.display = "none";
		}
	document.getElementById("containerDiff").style.display="block";
		if(indArray[i].textcontent.text != ""){
			//makes array of all the headerNames
			headerNames.push(indArray[i].textcontent.text);
		}
		//number of headers
		mainHeaders = headerNames.length;
		}
		document.getElementById('flightsLink').style.display = "none";
		document.getElementById('headerLink').style.display = "none";

		document.getElementById('headerLink').style.display = "block";
		if(parsedJson.Elements[0].textcontent.text == "DEPARTURES"){
			document.getElementById('linkValues').innerHTML = "Link X-Values";
		}
		else{
			document.getElementById('linkValues').innerHTML = "Link Y-Values";
			document.getElementById('flightsLink').style.display = "block";
		}


		//header text is shown
		document.getElementById("headerID").style.display="block";
		document.getElementById("flightsID").style.display="block";
		for(var c=0; c<mainHeaders; c++){
			document.getElementById("header"+(c+1)).innerHTML = headerNames[c].toUpperCase();
			document.getElementById("header"+(c+1)).style.display="block";
			document.getElementById("form"+(c+1)).style.display = "block";
			headerContentX = parsedJson.Elements[c].x;
			headerContentY = parsedJson.Elements[c].y;
			xArr.push(headerContentX);
			yArr.push(headerContentY);
			localStorage.xArr = xArr;
			document.getElementById("header"+(c+1)+"X").value = headerContentX;
			document.getElementById("header"+(c+1)+"Y").value = headerContentY;
		}

		//If the loaded JSON is 2.json
		if(parsedJson.Elements[0].textcontent.text != "DEPARTURES"){
			for(var c = 0; c<mainHeaders; c++){
				document.getElementById("form"+(c+4)).style.display = "block";
				flightsContentX = parsedJson.Elements[c+3].x;
				flightsContentY = parsedJson.Elements[c+3].y;
				xArr.push(headerContentX);
				yArr.push(headerContentY);
				document.getElementById("flights"+(c+1)+"X").value = flightsContentX;
				document.getElementById("flights"+(c+1)+"Y").value = flightsContentY;
			}
			//"Header text is shown"

	  }
		//else if loaded JSON is 3.json
		else{
			document.getElementById("form4").style.display = "block";
			document.getElementById("header4").style.display = "block";
			//MASTER DESTINATION (FIRST DESTINATION)
			document.getElementById("flights1X").value = parsedJson.Elements[2].x;
			document.getElementById("flights1Y").value = parsedJson.Elements[2].y;
		}
		deltaY = parsedJson.Elements[6].y - parsedJson.Elements[3].y;
		document.getElementById('deltaFlights').value = deltaY;
		document.getElementById('deltaFlightsText').style.display = "block";
		document.getElementById('defDeltaY').style.display = "inline";
		document.getElementById('deltaFlights').style.display = "inline";

	};
			document.getElementById('defDeltaY').onclick =() =>{
				document.getElementById('deltaFlights').value = 164;
				deltaY = 164;
			}

			document.getElementById('header1Y').onchange = function() {changeValues(2,3, 'header',1,"Y")};
			document.getElementById('header2Y').onchange = function() {changeValues(1,3, 'header',2,"Y")};
			document.getElementById('header3Y').onchange = function() {changeValues(1,2, 'header',3,"Y")};

			document.getElementById('header1X').onchange = function() {changeValues(2,3, "header",1,"X")};
			document.getElementById('header2X').onchange = function() {changeValues(1,3, "header",2,"X")};
			document.getElementById('header3X').onchange = function() {changeValues(1,2, "header",3,"X")};

			document.getElementById('flights1Y').onchange = function() {changeValues(2,3, "flights",1,"Y")};
			document.getElementById('flights2Y').onchange = function() {changeValues(1,3, "flights",2,"Y")};
			document.getElementById('flights3Y').onchange = function() {changeValues(1,2, "flights",3,"Y")};









			function changeValues(one, two, header, three, xOrY){
				if(parsedJson.Elements[0].textcontent.text != "DEPARTURES" && document.getElementById('headerLink').checked && header=="header" && xOrY=="Y"){
					document.getElementById(header+one+"Y").value = document.getElementById(header+three+xOrY).value;
					document.getElementById(header+two+"Y").value = document.getElementById(header+three+xOrY).value;
				}
				else if(parsedJson.Elements[0].textcontent.text == "DEPARTURES" && document.getElementById('headerLink').checked && header=="header" && xOrY=="X"){
					var old;
					//gets old xArr values and finds difference to subtract or add to the other number field
					if(three == 1){
						old = xArr[0]; //X1 Changed
						xArr[0] = document.getElementById('header1X').value;

					}
					else if(three == 2){
						old = xArr[1]; //X2 changed
						xArr[1] = document.getElementById('header2X').value;
					}
					else{
						old = xArr[2]; //X2 changed
						xArr[2] = document.getElementById('header3X').value;
					}
					 var diff = document.getElementById(header+three+xOrY).value - old;
					document.getElementById(header+one+"X").value = parseInt(document.getElementById(header+one+"X").value) + diff;
					document.getElementById(header+two+"X").value = parseInt(document.getElementById(header+two+"X").value) + diff;
				}
				else if(parsedJson.Elements[0].textcontent.text != "DEPARTURES" && document.getElementById('flightsLink').checked && header=="flights" && xOrY=="Y"){
					document.getElementById(header+one+"Y").value = document.getElementById(header+three+"Y").value;
					document.getElementById(header+two+"Y").value = document.getElementById(header+three+"Y").value;

				}

			}






		function changeOtherYsHeader(one, two, headerVal, x, y){
			if(parsedJson.Elements[0].textcontent.text != "DEPARTURES" && document.getElementById('headerLink').checked){
			document.getElementById("header"+one+"Y").value = document.getElementById(headerVal+y).value;
			document.getElementById("header"+two+"Y").value = document.getElementById(headerVal+y).value;
			}
			else if(parsedJson.Elements[0].textcontent.text == "DEPARTURES" && document.getElementById('headerLink').checked){
				document.getElementById("header"+one+"X").value = document.getElementById(headerVal+x).value;
				document.getElementById("header"+two+"X").value = document.getElementById(headerVal+x).value;
			}
		};

		function changeOtherYsFlights(one, two, headerVal, x, y){
			if(parsedJson.Elements[0].textcontent.text != "DEPARTURES" && document.getElementById('flightsLink').checked){
				document.getElementById("flights"+one+"Y").value = document.getElementById(headerVal+y).value;
				document.getElementById("flights"+two+"Y").value = document.getElementById(headerVal+y).value;
			}
			else if(parsedJson.Elements[0].textcontent.text == "DEPARTURES" && document.getElementById('flightsLink').checked){
				document.getElementById("flights"+one+"X").value = document.getElementById(headerVal+x).value;
				document.getElementById("flights"+two+"X").value = document.getElementById(headerVal+x).value;
			}
		};

		document.getElementById('deltaFlights').onchange = () =>{
			deltaY = document.getElementById('deltaFlights').value;
		}



	document.getElementById('makeChanges').onclick = () => {
		document.getElementById('makeChanges').value = "JSON FILE UPDATED";
		for(var element =0; element <mainHeaders; element++){
			parsedJson.Elements[element].x = parseInt(document.getElementById("header"+(element+1)+"X").value);
			parsedJson.Elements[element].y = parseInt(document.getElementById("header"+(element+1)+"Y").value);

		}
		var count = 0;
		for(var ind = 3; ind < parsedJson.Elements.length-3; ind+=3){
			for(var b =0; b<3; b++){
			parsedJson.Elements[ind+b].y =  parseInt(document.getElementById("flights1Y").value) + (deltaY* count);
			}
			count++;
		}
		if(parsedJson.Elements[0].textcontent.text == "DEPARTURES"){

			for(var json2 = 0; json2 < parsedJson.Elements.length-2; json2++){
				parsedJson.Elements[json2+2].x = parseInt(document.getElementById("flights1X").value);
				parsedJson.Elements[json2+2].y = parseInt(document.getElementById("flights1Y").value) + (deltaY*json2);
			}
		}

			var data = JSON.stringify(parsedJson, undefined, 2);
			fs.writeFileSync(filePath, data);
			return true;

	};
